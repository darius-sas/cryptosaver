﻿using CryptoSaver.Models;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Owin.Security.OAuth;
using System.Security.Permissions;

namespace CryptoSaver
{
	namespace Saving
	{
		/// <summary>
		/// Classe astratta che permette il salvataggio dei dati in diversi modi e il loro criptaggio.
		/// </summary>
		public abstract class Saver
		{
			private Crypter crypter;
			public AccountList Accounts;

			public Saver()
			{
				crypter = new Crypter("");
				Accounts = new AccountList();
			}
			
			protected abstract Stream GetSaveStream();
			protected abstract Stream GetLoadStream();

			public void Save()
			{
				XmlWriterSettings settings = new XmlWriterSettings();
				settings.Indent = true;
				settings.OmitXmlDeclaration = false;
				XmlSerializer xml = new XmlSerializer(typeof(AccountList));
				
				// Scrivo in memoria lo stream degli oggetti serializzati e poi lo critto e scrivo sull'output.
				using (MemoryStream cache = new MemoryStream())
				using (XmlWriter xmlWriter = XmlWriter.Create(cache, settings))
				using (Stream dest = GetSaveStream())
				{
					xml.Serialize(xmlWriter, Accounts);
					xmlWriter.Flush();
					cache.Position = 0;
					crypter.EncryptStream(cache, dest);
				}
				crypter.Close();
			}

			public Account[] Load()
			{
				XmlReaderSettings settings = new XmlReaderSettings();
				XmlRootAttribute xRoot = new XmlRootAttribute();
				xRoot.ElementName = "AccountList";
				xRoot.IsNullable = true;
				XmlSerializer xml = new XmlSerializer(typeof(AccountList), xRoot);
				
				using (MemoryStream cache = new MemoryStream())
				using (Stream source = GetLoadStream())
				{
					if (source.Length > 1)
					{
						crypter.DecryptStream(source, cache);
						cache.Position = 0;
						StreamReader reader = new StreamReader(cache);
						Accounts = (AccountList)xml.Deserialize(reader);
					}
					else
					{
						Accounts = new AccountList();
					}
				}
				crypter.Close();
				return Accounts.Accounts;
			}

			public void SetPassword(String psw)
			{
				crypter = new Crypter(psw);
			}

			private static bool IsEmptyStream(Stream myStream)
			{
				return myStream.Length == 0 ? true : false;
			}
		}
		
		/// <summary>
		/// Salva i dati su file fornendo a Saver gli stream per leggere e scrivere il file.
		/// </summary>
		public class FileSaver : Saver
		{
			private const string name = "passwords.csp";
			private string _savingPath;

			public string FilePath
			{
				get { return _savingPath; } 
				set
				{
					if (value.LastIndexOf('\\') != value.Length - 1)
					{
						_savingPath = value + '\\' + name;
					}
					else
					{
						_savingPath = name;
					}
				}
			}

			public FileSaver() : this(String.Empty) { }

			public FileSaver(string path) : base ()
			{
				this.FilePath = path;	
			}

			sealed protected override Stream GetSaveStream()
			{
				return File.OpenWrite(FilePath);
			}

			sealed protected override Stream GetLoadStream()
			{
				if (!File.Exists(FilePath))
				{
					File.Create(FilePath).Close();
				}
                FileStream fs = File.OpenRead(FilePath);
				return fs;
			}
		}

		public sealed class Crypter
		{

			RijndaelManaged rijndael;
			ICryptoTransform encryptor;
			ICryptoTransform decryptor;

			byte[] IV;
			byte[] key;
			const int IVLENGTH = 16; // 16 * 8 = 128 bit!
			const int KEYLENGTH = 32;

			CryptoStream cs;
			StreamReader reader;
			StreamWriter writer;

			/// <summary>
			/// Creates the CrytpographStreamer object.
			/// </summary>
			/// <param name="key">UTF-8 key.</param>
			public Crypter(String psw)
			{
				// Creates IV and the Key based on UTF-8 encoded string
				this.IV = CreateKey(psw, IVLENGTH);
				this.key = CreateKey(psw, KEYLENGTH);

				// Inits the cryptographing tools
				rijndael = new RijndaelManaged();
				encryptor = rijndael.CreateEncryptor(key, IV);
				decryptor = rijndael.CreateDecryptor(key, IV);
			}

			/// <summary>
			/// Gets a decrypted stream and encrypts it.
			/// </summary>
			/// <param name="s">A plaintext stream.</param>
			/// <returns>A ciphertext stream.</returns>
			public byte[] EncryptString(String s)
			{
				byte[] inputBytes = Encoding.UTF8.GetBytes(s);
				return encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
			}

			/// <summary>
			/// Gets an encrypted character stream and returns a decrypted characters stream.
			/// </summary>
			/// <param name="s">Stream to decrypt.</param>
			/// <returns>A stream containing the plaintext.</returns>
			public String DecryptBytes(byte[] b)
			{
				byte[] outputBytes = decryptor.TransformFinalBlock(b, 0, b.Length);
				return Encoding.UTF8.GetString(outputBytes);
			}

			/// <summary>
			/// Encrypts an input stream and writes data on an output one. Does NOT close any stream. 
			/// You need to call Close() method in order to close all underlying streams.
			/// </summary>
			/// <param name="sourceStream">The stream where to read data.</param>
			/// <param name="destStream">The strean where to write data.</param>
			public void EncryptStream(Stream sourceStream, Stream destStream)
			{
				reader = new StreamReader(sourceStream, Encoding.UTF8);
				cs = new CryptoStream(destStream, encryptor, CryptoStreamMode.Write);
				writer = new StreamWriter(cs, Encoding.UTF8);
				String s = reader.ReadToEnd();
				writer.Write(s);
				writer.Flush();
				cs.FlushFinalBlock();
			}

			/// <summary>
			/// Decrypts an input stream and writes data on an output one.Does NOT close any stream. 
			/// You need to call Close() method in order to close all underlying streams.
			/// </summary>
			/// <param name="sourceStream">The stream where to read data.</param>
			/// <param name="destStream">The strean where to write data.</param>
			public void DecryptStream(Stream sourceStream, Stream destStream)
			{
				writer = new StreamWriter(destStream, Encoding.UTF8);
				cs = new CryptoStream(sourceStream, decryptor, CryptoStreamMode.Read);
				reader = new StreamReader(cs, Encoding.UTF8);
				writer.Write(reader.ReadToEnd());
				writer.Flush();
			}

			/// <summary>
			/// Closes all the streams used by this object.
			/// </summary>
			public void Close()
			{
				if (cs != null)
					cs.Close();
				if (reader != null)
					reader.Close();
				if (writer != null)
					writer.Close();
			}

			/// <summary>
			/// Creates Key or IV bytes based on a password.
			/// </summary>
			/// <param name="password">The password to create the byte array.</param>
			/// <param name="length">The length desired in bytes.</param>
			/// <returns>The array of bytes.</returns>
			private static byte[] CreateKey(string password, int length)
			{
				var salt = new byte[] { 0x01, 0x02, 0x23, 0x34, 0x37, 0x48, 0x24, 0x63, 0x99, 0x04 };

				const int Iterations = 1000;
				using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt, Iterations))
					return rfc2898DeriveBytes.GetBytes(length);
			}
		}

		public class OneDriveSaver : Saver
		{
			public OneDriveSaver() : base()
			{

			}
			sealed protected override Stream GetSaveStream()
			{
				
				throw new NotImplementedException();
			}

			sealed protected override Stream GetLoadStream()
			{
				throw new NotImplementedException();
			}
		}
		
	}
}
