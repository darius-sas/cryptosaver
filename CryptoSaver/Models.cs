﻿using System;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

namespace CryptoSaver
{
	namespace Models
	{
		[Serializable()]
		public class Account
		{
			[XmlElement("Name")]
			public string Name { get; set; }
			[XmlElement("Username")]
			public string Username { get; set; }
			[XmlElement("Password")]
			public string Password { get; set; }
			[XmlElement("Email")]
			public string Email { get; set; }
			[XmlElement("Description")]
			public string Description { get; set; }

			public Account() { }

			public Account(string name, string username, string password, string email, string description = "")
			{
				Name = name;
				Username = username;
				Password = password;
				Email = email;
				Description = description;
			}

			public override string ToString()
			{
				return Name;
			}
		}
		[Serializable()]
		[XmlRoot("AccountList")]
		public class AccountList
		{
			private List<Account> _accList;
			[XmlArray("Accounts")]
			[XmlArrayItem("Account", typeof(Account))]
			public Account[] Accounts 
			{ 
				get
				{
					return _accList.ToArray();
				}
				set
				{
					Account[] a = value;
					if(a == null)
					{
						throw new Exception("Errore durante la conversione in List");
					}
					_accList = new List<Account>(a);
				}
			}

			public AccountList()
			{
				Accounts = new Account[0];
			}

			public void AddAccount(Account a)
			{
				if (a == null)
				{
					throw new ArgumentNullException("Parameter \"a\" cannot be null.");
				}
				_accList.Add(a);
			}
		}
	}
}
