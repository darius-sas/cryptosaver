﻿using CryptoSaver.Models;
using CryptoSaver.Saving;
using System;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace CryptoSaver
{
	/// <summary>
	/// Logica di interazione per MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		SystemWrapper SYSTEM;
		
		public MainWindow()
		{
			Window1 user = new Window1();
			user.ShowDialog();
			if (user.DialogResult.HasValue && !user.DialogResult.Value)
				Application.Current.Shutdown();

			bool? dialogResult = user.DialogResult;
			if (dialogResult == true)
			{
				SecureString psw = user.GetPassword();
				SYSTEM = new SystemWrapper(new FileSaver(), psw, user.IsNewUser);
				user.Clear(); // Elimina la password (non cifrata) dalla memoria

				// SYSTEM.Authenticate();
				if (SYSTEM.IsAutenticated)
				{
					InitializeComponent();
					Account[] data = SYSTEM.LoadData();
					AccountNamesComboBox.ItemsSource = data;
					AccountNamesComboBox.SelectionChanged += AccountNamesComboBox_SelectionChanged;
				}
				else
				{
					MessageBox.Show("Password Errata.");
					Application.Current.Shutdown();
				}
			}
		}
		
		private void AddButton_Click(object sender, RoutedEventArgs e)
		{
			if (AccountNameTextBox == null || AccountNameTextBox.Text.Equals(""))
			{
				ShowResultNotification("Nome account vuoto!", new SolidColorBrush(Color.FromRgb(225, 0, 58)));
			}
			else
			{
				SYSTEM.SaveData(AccountNameTextBox.Text, UsernameTextBox.Text, PasswordTextBox.Text, EmailTextBox.Text, DescriptionTextBox.Text);
				ShowResultNotification("Aggiunto", new SolidColorBrush(Color.FromRgb(0, 225, 58)));
				AccountNamesComboBox.ItemsSource = SYSTEM.LoadData();
				AccountNameTextBox.Text = "";
				UsernameTextBox.Text = "";
				PasswordTextBox.Text = "";
				EmailTextBox.Text = "";
				DescriptionTextBox.Text = "";
			}
		}

		/// <summary>
		/// Mostra un messaggio all'utente.
		/// </summary>
		/// <param name="msg">Messaggio da mostrare.</param>
		/// <param name="background">Colore dello sfondo.</param>
		private void ShowResultNotification(String msg, Brush background)
		{
			AggiuntoLabel.Content = msg;
			AggiuntoLabel.Background = background;
			AggiuntoLabel.Visibility = System.Windows.Visibility.Visible;
			AggiuntoLabel.UpdateLayout();
			DoubleAnimation animation = new DoubleAnimation();
			animation.From = 0.0d;
			animation.To = 1.0d;
			animation.Duration = new Duration(new TimeSpan(0, 0, 1));
			DependencyProperty dp = Label.OpacityProperty;
			AggiuntoLabel.BeginAnimation(dp, animation);
			animation.Duration = new Duration(new TimeSpan(0, 0, 2));
			animation.From = 1.0d;
			animation.To = 0.0d;
			animation.Completed += animation_Completed;
			AggiuntoLabel.BeginAnimation(dp, animation);
		}

		void animation_Completed(object sender, EventArgs e)
		{
			AggiuntoLabel.Visibility = System.Windows.Visibility.Hidden;
		}

		void AccountNamesComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			ComboBox s = (ComboBox)sender;
			if (s.SelectedIndex != -1)
			{
				Account acc = (Account)s.SelectedItem;
				ShowUsernameLabel.Content = acc.Username;
				ShowPasswordLabel.Content = acc.Password;
				ShowEmailLabel.Content = acc.Email;
				ShowDescriptionLabel.Content = acc.Description;
			}
		}

		private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			DragMove();
		}

		private void Button_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			((Button)sender).Background = new SolidColorBrush(Color.FromRgb(218, 30, 11));
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.Shutdown();
		}

		private void CopyToClipboard_Click(object sender, RoutedEventArgs e)
		{
			Clipboard.SetText(ShowPasswordLabel.Content.ToString());
		}

		private const string PASSWORD_SHOW_COLOR = "#FFE2E2E2";
		
	}
}
