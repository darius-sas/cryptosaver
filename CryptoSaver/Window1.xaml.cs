﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CryptoSaver
{
	/// <summary>
	/// Logica di interazione per Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{

		public bool IsNewUser { get {return (bool)NewUserCheckBox.IsChecked.Value; } private set{} }

		public Window1()
		{
			InitializeComponent();
			pswBox.Focus();
		}

		public void Clear()
		{
			pswBox.Password = null;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (GetPassword() == null || GetPassword().Length < 1)
			{
				DialogResult = null;
			}
			else
			{
				DialogResult = true;
			}
		}

		public SecureString GetPassword() 
		{
			SecureString s = new SecureString();
			foreach (var c in pswBox.Password.ToCharArray())
				s.AppendChar(c);
			return s;
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}

		private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			DragMove();
		}
	}
}
