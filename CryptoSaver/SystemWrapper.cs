﻿using CryptoSaver.Models;
using CryptoSaver.Saving;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Security.Cryptography;
using System.Security;

namespace CryptoSaver
{
	public sealed class SystemWrapper
	{
		public const uint MAX_USERS = 10;

		private Saver s;
		private byte[] _inputPasswordHash;
		private byte[] InputPasswordHash { 
			get { return _inputPasswordHash; }
			set { _inputPasswordHash = new SHA1Managed().ComputeHash(value); } 
		}

		private const string userPasswordFilePattern = "userProfile?.csp";
		private uint userId;
		public bool IsAutenticated { get; private set; }

		public SystemWrapper(Saver s, SecureString password, bool newUser)
		{
			InputPasswordHash = Encoding.UTF8.GetBytes(password.ToString());
			password.Clear();
			IsAutenticated = false;
			this.s = s;

			InitUserInfo(newUser);
		}

		/// <summary>
		/// Initiliazes user info and file.
		/// <param name="newUser">Create a new user?</param>
		/// </summary>
		private void InitUserInfo(bool newUser)
		{
			string cryptoDir = Environment.GetEnvironmentVariable("USERPROFILE") + @"\Documents\cryptosaver\";
			cryptoDir = ".";
			if (!Directory.Exists(cryptoDir))
			{
				DirectoryInfo di = Directory.CreateDirectory(cryptoDir);
				di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
			}

			uint i = 0, countUId = 0;
			IsAutenticated = false;
			string userFile = cryptoDir + userPasswordFilePattern.Replace("?", countUId.ToString());
			while (!IsAutenticated && i++ < MAX_USERS)
			{
				userFile = cryptoDir + userPasswordFilePattern.Replace("?", countUId.ToString());
				if (File.Exists(userFile) && !newUser) // entra solo se non sono un nuovo utente
				{
					countUId++;
					byte[] fileBytes = File.ReadAllBytes(userFile);
					if (fileBytes.Length > 100)
						throw new FileLoadException("Formato file utente scorretto.");
					if (fileBytes.SequenceEqual(InputPasswordHash))
					{
						IsAutenticated = true;
						userId = countUId;
					}
				}
			}
			if (newUser)
			{ // crea il file per il nuovo utente
				if (File.Exists(userFile))
					throw new InvalidOperationException("Raggiunto il numero massimo di utenti");
				using (FileStream fs = File.Open(userFile, FileMode.Create))
					fs.Write(InputPasswordHash, 0, InputPasswordHash.Length);
				IsAutenticated = true;
				userId = countUId;
			}
		}

		public void Authenticate()
		{
			try
			{
				// IsAutenticated = true;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}



		public Account[] LoadData()
		{
			try
			{
				if (!IsAutenticated)
				{
					throw new System.Security.Authentication.AuthenticationException("Tentativo di caricamento dei dati senza autenticazione.");
				}
				return s.Load();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
				return null;
			}
		}

		public void SaveData(string name, string username, string password, string email, string description)
		{
			Account a = new Account(name, username, password, email, description);
			try
			{
				s.Accounts.AddAccount(a);
				s.Save();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		public static byte[] GetBytes(string str)
		{
			byte[] bytes = new byte[str.Length];
			char[] chars = str.ToCharArray();
			for (int i = 0; i < bytes.Length; i++)
			{
				bytes[i] = Convert.ToByte(chars[i]);
			}
			return bytes;
		}
	}
}
